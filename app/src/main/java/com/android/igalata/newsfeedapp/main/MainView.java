package com.android.igalata.newsfeedapp.main;

import android.support.v4.app.Fragment;

public interface MainView {
    void changeFragment(Fragment fragment);

    void initTabs();
}
