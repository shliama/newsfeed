package com.android.igalata.newsfeedapp.web;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.android.igalata.newsfeedapp.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class NewsInfoActivity extends AppCompatActivity implements NewsInfoView {

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    private NewsInfoPresenter mPresenter;
    private String mUrl;
    private String mHeadline;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_info);
        ButterKnife.bind(this);
        mPresenter = new NewsInfoPresenter(this);
        mPresenter.onCreate();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return mPresenter.onOptionsItemSelected(item);
    }

    @Override
    public void initToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(mHeadline);
    }

    @Override
    public void showNewsInfoFragment() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, NewsInfoFragment.newInstance(mUrl)).commit();
    }

    @Override
    public void goBack() {
        onBackPressed();
    }

    @Override
    public void getExtra() {
        mUrl = getIntent().getStringExtra("url");
        mHeadline = getIntent().getStringExtra("headline");
    }
}
