package com.android.igalata.newsfeedapp.bookmarks;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.igalata.newsfeedapp.R;
import com.android.igalata.newsfeedapp.model.News;
import com.android.igalata.newsfeedapp.utils.UiUtils;
import com.android.igalata.newsfeedapp.web.NewsInfoActivity;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class BookmarksFragment extends Fragment implements BookmarksView {
    @Bind(R.id.bookmarks_list)
    RecyclerView bookmarksList;
    private BookmarksPresenter mPresenter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bookmarks, container, false);
        ButterKnife.bind(this, view);
        mPresenter = new BookmarksPresenter(this);
        mPresenter.onCreate();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.onResume();
    }

    @Override
    public void initList() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        bookmarksList.setLayoutManager(layoutManager);
    }

    @Override
    public void refresh(List<News> news) {
        bookmarksList.setAdapter(new BookmarksAdapter(getContext(), news, mPresenter, mPresenter));
    }

    @Override
    public void notifyDataSetChanged() {
        bookmarksList.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void goToWebActivity(News news) {
        Intent intent = new Intent(getContext(), NewsInfoActivity.class);
        intent.putExtra("url", news.getWebUrl());
        intent.putExtra("headline", news.getHeadLine());
        startActivity(intent);
    }

    @Override
    public void showMessage(int textRes) {
        UiUtils.showSnackbarWithCancelAction(getView(), textRes, mPresenter);
    }
}
