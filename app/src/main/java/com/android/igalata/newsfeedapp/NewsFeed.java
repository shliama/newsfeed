package com.android.igalata.newsfeedapp;


import android.app.Application;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class NewsFeed extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        setRealmConfig();
    }

    private void setRealmConfig() {
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(this)
                .deleteRealmIfMigrationNeeded().name(Const.DB_NAME).build();
        Realm.setDefaultConfiguration(realmConfiguration);
    }
}
