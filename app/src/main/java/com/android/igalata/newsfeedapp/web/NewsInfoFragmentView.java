package com.android.igalata.newsfeedapp.web;

public interface NewsInfoFragmentView {
    void setupWebView();

    void saveArguments();
}
