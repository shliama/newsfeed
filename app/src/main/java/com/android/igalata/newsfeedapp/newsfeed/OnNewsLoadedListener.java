package com.android.igalata.newsfeedapp.newsfeed;

import com.android.igalata.newsfeedapp.model.News;

import java.util.List;

public interface OnNewsLoadedListener {
    void onSuccess(List<News> news);

    void onError(String error);
}
