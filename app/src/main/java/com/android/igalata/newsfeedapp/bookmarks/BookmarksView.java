package com.android.igalata.newsfeedapp.bookmarks;

import com.android.igalata.newsfeedapp.model.News;

import java.util.List;

public interface BookmarksView {
    void initList();

    void refresh(List<News> news);

    void notifyDataSetChanged();

    void goToWebActivity(News news);

    void showMessage(int textRes);
}
