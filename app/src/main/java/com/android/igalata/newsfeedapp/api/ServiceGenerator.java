package com.android.igalata.newsfeedapp.api;

import com.android.igalata.newsfeedapp.Const;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;

import io.realm.RealmObject;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

public class ServiceGenerator {
    private static OkHttpClient httpClient = new OkHttpClient();
    private static Retrofit.Builder builder;
    private static Gson gson;
    private static ExclusionStrategy strategy = new ExclusionStrategy() {
        @Override
        public boolean shouldSkipField(FieldAttributes f) {
            return f.getDeclaringClass().equals(RealmObject.class);
        }

        @Override
        public boolean shouldSkipClass(Class<?> clazz) {
            return false;
        }
    };

    public static <S> S createService(Class<S> serviceClass) {
        initGson();
        initBuilder();
        Retrofit retrofit = builder.client(httpClient).build();
        return retrofit.create(serviceClass);
    }

    private static void initBuilder() {
        builder = new Retrofit.Builder()
                .baseUrl(Const.ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create(gson));
    }

    private static void initGson() {
        gson = new GsonBuilder()
                .setExclusionStrategies(strategy)
                .create();
    }
}
