package com.android.igalata.newsfeedapp.api;

import com.android.igalata.newsfeedapp.model.NewsResponse;

import retrofit.Call;
import retrofit.http.GET;

public interface NewsService {
    @GET("/feeds/newsdefaultfeeds.cms?feedtype=sjson")
    Call<NewsResponse> getNews();
}
