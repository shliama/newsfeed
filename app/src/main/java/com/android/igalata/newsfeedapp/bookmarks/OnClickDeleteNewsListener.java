package com.android.igalata.newsfeedapp.bookmarks;


import com.android.igalata.newsfeedapp.model.News;

public interface OnClickDeleteNewsListener {
    void onClickDelete(News news);
}
