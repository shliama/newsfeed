package com.android.igalata.newsfeedapp.newsfeed;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.android.igalata.newsfeedapp.OnNewsItemClickListener;
import com.android.igalata.newsfeedapp.R;
import com.android.igalata.newsfeedapp.model.News;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.NewsViewHolder> {

    private List<News> mNews;
    private Context mContext;
    private OnNewsLikedListener mOnNewsLikedListener;
    private OnNewsItemClickListener mOnNewsItemClickedListener;

    public NewsAdapter(Context context, List<News> news, OnNewsLikedListener onNewsLikedListener,
                       OnNewsItemClickListener onNewsItemClickListener) {
        mNews = news;
        mContext = context;
        mOnNewsLikedListener = onNewsLikedListener;
        mOnNewsItemClickedListener = onNewsItemClickListener;
    }

    @Override
    public NewsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_news, parent, false);
        return new NewsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NewsViewHolder holder, int position) {
        News news = mNews.get(position);
        holder.date.setText(news.getDate());
        holder.headLine.setText(news.getHeadLine());
        Picasso.with(mContext).load(mNews.get(position).getImage().getPhoto())
                .networkPolicy(NetworkPolicy.OFFLINE).into(holder.image, new Callback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError() {
                Picasso.with(mContext).load(mNews.get(position).getImage().getPhoto()).into(holder.image);
            }
        });
        holder.buttonLike.setTag(mNews.get(position));
        holder.image.setTag(mNews.get(position));
        holder.buttonLike.setChecked(mNews.get(position).isSaved());
    }

    @Override
    public int getItemCount() {
        return mNews.size();
    }

    public class NewsViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.text_date)
        TextView date;
        @Bind(R.id.text_headline)
        TextView headLine;
        @Bind(R.id.image)
        ImageView image;
        @Bind(R.id.button_like)
        ToggleButton buttonLike;

        public NewsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.button_like)
        public void onNewsLiked(ToggleButton button) {
            News news = (News) button.getTag();
            if (button.isChecked()) {
                mOnNewsLikedListener.onNewsLiked(news);
            } else {
                mOnNewsLikedListener.onNewsDisliked(news);
            }
        }

        @OnClick(R.id.image)
        public void onClick(View view) {
            mOnNewsItemClickedListener.onClick((News) view.getTag());
        }
    }
}
