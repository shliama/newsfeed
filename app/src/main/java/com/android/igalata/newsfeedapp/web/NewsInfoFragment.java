package com.android.igalata.newsfeedapp.web;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.android.igalata.newsfeedapp.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class NewsInfoFragment extends Fragment implements NewsInfoFragmentView {
    @Bind(R.id.web_view)
    WebView webView;
    private String mNewsUrl;
    private NewsInfoFragmentPresenter mPresenter;

    public static NewsInfoFragment newInstance(String newsUrl) {
        NewsInfoFragment fragment = new NewsInfoFragment();
        Bundle bundle = new Bundle();
        bundle.putString("url", newsUrl);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new NewsInfoFragmentPresenter(this);
        mPresenter.onCreate();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_news_info, container, false);
        ButterKnife.bind(this, view);
        mPresenter.onCreateView();
        return view;
    }

    @Override
    public void setupWebView() {
        webView.loadUrl(mNewsUrl);
        webView.setWebViewClient(new WebViewClient());
    }

    @Override
    public void saveArguments() {
        mNewsUrl = getArguments().getString("url");
    }
}
