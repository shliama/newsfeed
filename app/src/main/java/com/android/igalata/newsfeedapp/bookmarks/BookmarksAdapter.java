package com.android.igalata.newsfeedapp.bookmarks;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.igalata.newsfeedapp.OnNewsItemClickListener;
import com.android.igalata.newsfeedapp.R;
import com.android.igalata.newsfeedapp.model.News;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BookmarksAdapter extends RecyclerView.Adapter<BookmarksAdapter.BookmarkViewHolder> {
    private List<News> mNews;
    private Context mContext;
    private OnClickDeleteNewsListener mOnClickDeleteListener;
    private OnNewsItemClickListener mOnClickListener;

    public BookmarksAdapter(Context context, List<News> news, OnClickDeleteNewsListener listener, OnNewsItemClickListener onClickListener) {
        mNews = news;
        mContext = context;
        mOnClickDeleteListener = listener;
        mOnClickListener = onClickListener;
    }

    @Override
    public BookmarkViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.list_item_bookmarks, parent, false);
        return new BookmarkViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BookmarkViewHolder holder, int position) {
        holder.headline.setText(mNews.get(position).getHeadLine());
        holder.date.setText(mNews.get(position).getDate());
        Picasso.with(mContext).load(mNews.get(position).getImage().getPhoto()).networkPolicy(NetworkPolicy.OFFLINE).into(holder.image, new Callback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError() {
                Picasso.with(mContext).load(mNews.get(position).getImage().getPhoto()).into(holder.image);
            }
        });
        holder.buttonDelete.setTag(mNews.get(position));
        holder.root.setTag(mNews.get(position));
    }

    @Override
    public int getItemCount() {
        return mNews.size();
    }

    class BookmarkViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.text_headline)
        TextView headline;
        @Bind(R.id.text_date)
        TextView date;
        @Bind(R.id.image)
        ImageView image;
        @Bind(R.id.button_delete)
        ImageButton buttonDelete;
        @Bind((R.id.root))
        View root;

        public BookmarkViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.button_delete)
        public void onClickDelete(View view) {
            mOnClickDeleteListener.onClickDelete((News) view.getTag());
        }

        @OnClick(R.id.root)
        public void onItemClicked(View view) {
            mOnClickListener.onClick((News) view.getTag());
        }
    }
}
