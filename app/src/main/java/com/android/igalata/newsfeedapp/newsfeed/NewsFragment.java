package com.android.igalata.newsfeedapp.newsfeed;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.igalata.newsfeedapp.R;
import com.android.igalata.newsfeedapp.model.News;
import com.android.igalata.newsfeedapp.utils.UiUtils;
import com.android.igalata.newsfeedapp.web.NewsInfoActivity;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class NewsFragment extends Fragment implements NewsView {
    @Bind(R.id.news_list)
    RecyclerView newsList;
    @Bind(R.id.swipe_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    private NewsPresenter mPresenter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_news, container, false);
        ButterKnife.bind(this, view);
        mPresenter = new NewsPresenter(this);
        mPresenter.onCreate();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.onResume();
    }

    @Override
    public void showProgress() {
        swipeRefreshLayout.post(() -> swipeRefreshLayout.setRefreshing(true));
    }

    @Override
    public void hideProgress() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void initList() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        newsList.setLayoutManager(layoutManager);
    }

    @Override
    public void refresh(List<News> news) {
        newsList.setAdapter(new NewsAdapter(getContext(), news, mPresenter, mPresenter));
        newsList.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void showError(String error) {
        UiUtils.showSnackbar(getView(), error);
    }

    @Override
    public void initSwipeRefreshLayout() {
        swipeRefreshLayout.setColorSchemeResources(R.color.color_primary, R.color.color_accent, R.color.color_primary_dark);
        swipeRefreshLayout.setOnRefreshListener(mPresenter::onRefresh);
    }

    @Override
    public void goToWebActivity(News news) {
        Intent intent = new Intent(getContext(), NewsInfoActivity.class);
        intent.putExtra("url", news.getWebUrl());
        intent.putExtra("headline", news.getHeadLine());
        startActivity(intent);
    }

    @Override
    public void showMessage(int textRes) {
        UiUtils.showSnackbar(getView(), textRes);
    }
}
