package com.android.igalata.newsfeedapp.main;

import com.android.igalata.newsfeedapp.bookmarks.BookmarksFragment;
import com.android.igalata.newsfeedapp.newsfeed.NewsFragment;

public class MainPresenter {
    private MainView mView;

    public MainPresenter(MainView view) {
        this.mView = view;
    }

    public void onCreate() {
        mView.initTabs();
        onTabSelected(0);
    }

    public void onTabSelected(int position) {
        mView.changeFragment(position == 0 ? new NewsFragment() : new BookmarksFragment());
    }
}
