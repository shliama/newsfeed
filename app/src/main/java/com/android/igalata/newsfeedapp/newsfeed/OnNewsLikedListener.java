package com.android.igalata.newsfeedapp.newsfeed;

import com.android.igalata.newsfeedapp.model.News;

public interface OnNewsLikedListener {
    void onNewsLiked(News news);

    void onNewsDisliked(News news);
}
