package com.android.igalata.newsfeedapp.newsfeed;

import com.android.igalata.newsfeedapp.OnNewsItemClickListener;
import com.android.igalata.newsfeedapp.R;
import com.android.igalata.newsfeedapp.model.News;

import java.util.List;

public class NewsPresenter implements OnNewsLoadedListener, OnNewsLikedListener, OnNewsItemClickListener {
    private NewsView mView;
    private NewsInteractor mInteractor;

    public NewsPresenter(NewsView view) {
        this.mView = view;
        mInteractor = new NewsInteractor();
    }

    public void onCreate() {
        mView.initList();
        mView.initSwipeRefreshLayout();
    }

    public void onResume() {
        onRefresh();
    }

    public void onRefresh() {
        mView.showProgress();
        mInteractor.getNews(this);
    }

    @Override
    public void onSuccess(List<News> news) {
        mView.hideProgress();
        mView.refresh(news);
    }

    @Override
    public void onError(String error) {
        mView.hideProgress();
        mView.showError(error);
    }

    @Override
    public void onNewsLiked(News news) {
        mInteractor.saveNews(news);
        mView.showMessage(R.string.text_bookmark_saved);
    }

    @Override
    public void onNewsDisliked(News news) {
        mInteractor.deleteNews(news);
        mView.showMessage(R.string.text_bookmark_removed);
    }

    @Override
    public void onClick(News news) {
        mView.goToWebActivity(news);
    }
}
