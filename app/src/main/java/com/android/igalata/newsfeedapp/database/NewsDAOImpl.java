package com.android.igalata.newsfeedapp.database;

import com.android.igalata.newsfeedapp.model.News;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class NewsDAOImpl implements NewsDAO {

    @Override
    public List<News> getNews() {
        Realm realm = Realm.getDefaultInstance();
        return realm.allObjects(News.class);
    }

    @Override
    public void deleteNews(News news) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        RealmResults<News> results = realm.where(News.class)
                .equalTo("id", news.getId()).findAll();
        results.clear();
        realm.commitTransaction();
    }

    @Override
    public void addNews(News news) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(news);
        realm.commitTransaction();
        news.setSaved(true);
    }

    @Override
    public boolean contains(News news) {
        Realm realm = Realm.getDefaultInstance();
        return realm.where(News.class).equalTo("id", news.getId()).findFirst() != null;
    }
}
