package com.android.igalata.newsfeedapp.bookmarks;

import com.android.igalata.newsfeedapp.OnNewsItemClickListener;
import com.android.igalata.newsfeedapp.R;
import com.android.igalata.newsfeedapp.model.News;

public class BookmarksPresenter implements OnClickDeleteNewsListener, OnNewsItemClickListener,
        OnCancelClickListener {
    private BookmarksView mView;
    private BookmarksInteractor mInteractor;
    private News mRemovedNews;

    public BookmarksPresenter(BookmarksView view) {
        mView = view;
        mInteractor = new BookmarksInteractor();
    }

    public void onCreate() {
        mView.initList();
    }

    public void onResume() {
        mView.refresh(mInteractor.getBookmarks());
    }

    @Override
    public void onClickDelete(News news) {
        mRemovedNews = mInteractor.copy(news);
        mInteractor.deleteNews(news);
        mView.notifyDataSetChanged();
        mView.showMessage(R.string.text_bookmark_removed);
    }

    @Override
    public void onClick(News news) {
        mView.goToWebActivity(news);
    }

    @Override
    public void onClickCancel() {
        mInteractor.addNews(mRemovedNews);
        mView.notifyDataSetChanged();
    }
}
