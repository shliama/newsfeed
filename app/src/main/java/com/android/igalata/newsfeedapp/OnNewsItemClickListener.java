package com.android.igalata.newsfeedapp;

import com.android.igalata.newsfeedapp.model.News;

public interface OnNewsItemClickListener {
    void onClick(News news);
}
