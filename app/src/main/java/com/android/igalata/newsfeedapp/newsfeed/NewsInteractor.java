package com.android.igalata.newsfeedapp.newsfeed;

import com.android.igalata.newsfeedapp.api.NewsService;
import com.android.igalata.newsfeedapp.api.ServiceGenerator;
import com.android.igalata.newsfeedapp.database.NewsDAOImpl;
import com.android.igalata.newsfeedapp.model.News;
import com.android.igalata.newsfeedapp.model.NewsResponse;

import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class NewsInteractor {
    private NewsDAOImpl mDao;

    public NewsInteractor() {
        this.mDao = new NewsDAOImpl();
    }

    public void getNews(OnNewsLoadedListener listener) {
        NewsService service = ServiceGenerator.createService(NewsService.class);
        Call<NewsResponse> call = service.getNews();
        call.enqueue(new Callback<NewsResponse>() {
            @Override
            public void onResponse(Response<NewsResponse> response, Retrofit retrofit) {
                setSaved(response.body().getNews());
                listener.onSuccess(response.body().getNews());
            }

            @Override
            public void onFailure(Throwable t) {
                listener.onError(t.getMessage());
            }
        });
    }

    private void setSaved(List<News> news) {
        for (int i = 0; i < news.size(); ++i) {
            News newsItem = news.get(i);
            newsItem.setSaved(contains(newsItem));
        }
    }

    private boolean contains(News news) {
        return mDao.contains(news);
    }

    public void saveNews(News news) {
        mDao.addNews(news);
    }

    public void deleteNews(News news) {
        mDao.deleteNews(news);
    }
}
