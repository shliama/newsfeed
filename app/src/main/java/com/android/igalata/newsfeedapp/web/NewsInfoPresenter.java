package com.android.igalata.newsfeedapp.web;

import android.view.MenuItem;

public class NewsInfoPresenter {
    private NewsInfoView mView;

    public NewsInfoPresenter(NewsInfoView view) {
        this.mView = view;
    }

    public void onCreate() {
        mView.getExtra();
        mView.initToolbar();
        mView.showNewsInfoFragment();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            mView.goBack();
            return true;
        }
        return false;
    }
}
