package com.android.igalata.newsfeedapp.newsfeed;

import com.android.igalata.newsfeedapp.model.News;

import java.util.List;

public interface NewsView {
    void showProgress();

    void hideProgress();

    void initList();

    void refresh(List<News> news);

    void showError(String error);

    void initSwipeRefreshLayout();

    void goToWebActivity(News news);

    void showMessage(int textRes);
}
