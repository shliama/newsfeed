package com.android.igalata.newsfeedapp.database;

import com.android.igalata.newsfeedapp.model.News;

import java.util.List;

public interface NewsDAO {
    List<News> getNews();

    void deleteNews(News news);

    void addNews(News news);

    boolean contains(News news);
}
