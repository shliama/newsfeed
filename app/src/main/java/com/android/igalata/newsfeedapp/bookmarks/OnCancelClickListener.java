package com.android.igalata.newsfeedapp.bookmarks;

public interface OnCancelClickListener {
    void onClickCancel();
}
