package com.android.igalata.newsfeedapp.bookmarks;

import com.android.igalata.newsfeedapp.database.NewsDAOImpl;
import com.android.igalata.newsfeedapp.model.News;

import java.util.List;

public class BookmarksInteractor {
    private NewsDAOImpl mDao;

    public BookmarksInteractor() {
        mDao = new NewsDAOImpl();
    }

    public List<News> getBookmarks() {
        return mDao.getNews();
    }

    public void deleteNews(News news) {
        mDao.deleteNews(news);
    }

    public void addNews(News news) {
        mDao.addNews(news);
    }

    public News copy(News news) {
        News copy = new News();
        copy.setHeadLine(news.getHeadLine());
        copy.setDate(news.getDate());
        copy.setId(news.getId());
        copy.setImage(news.getImage());
        copy.setWebUrl(news.getWebUrl());
        return copy;
    }
}
