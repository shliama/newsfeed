package com.android.igalata.newsfeedapp.web;

public class NewsInfoFragmentPresenter {
    private NewsInfoFragmentView mView;

    public NewsInfoFragmentPresenter(NewsInfoFragmentView view) {
        this.mView = view;
    }

    public void onCreate() {
        mView.saveArguments();
    }

    public void onCreateView() {
        mView.setupWebView();
    }
}
