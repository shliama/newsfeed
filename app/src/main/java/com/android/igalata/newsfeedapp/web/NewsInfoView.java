package com.android.igalata.newsfeedapp.web;

public interface NewsInfoView {
    void initToolbar();

    void showNewsInfoFragment();

    void goBack();

    void getExtra();
}
